#include<iostream>
using namespace std;

template <class T>
void swap_values(T& variable1, T& variable2)
{
	T temp;
	temp = variable1;
	variable1 = variable2;
	variable2 = temp;
}


template <class T>
int index_of_smallest(const T a[], int start_index, int number_used)
{
	int min = a[start_index];
	int index_of_min = start_index;
	for (int index = start_index + 1; index < number_used; index++)
		if (a[index] < min)
		{
			min = a[index];
			index_of_min = index;
		}
	return index_of_min;
}

template <class T>
void sort(T a[], int number_used)
{
	int index_of_next_smallest;
	for (int index = 0; index < number_used - 1; index++)
	{
		index_of_next_smallest = index_of_smallest(a, index, number_used);
		swap_values(a[index], a[index_of_next_smallest]);
	}
}




int main() {
	cout << "Integer sorting " << endl; //sort int
	int a[5] = { 5,3,6,8,1 };
	cout << "Original array : ";
	for (int i = 0; i < 5; i++)
	{
		cout << "[" << a[i] << "] ";
	}
	sort(a, 5);
	cout <<"\nSorted array : ";
	for (int i = 0; i < 5; i++) 
	{
		cout << "[" << a[i] << "] ";
	}
	cout << endl;


	cout << "\n\nCharacter sorting" << endl; //sort char
	char b[5] = { 'B','H','Z','M','A' };
	cout << "Original array : ";
	for (int i = 0; i < 5; i++)
	{
		cout << "[" << b[i] << "] ";
	}
	sort(b, 5);
	cout << "\nSorted array : ";
	for (int i = 0; i < 5; i++) 
	{
		cout << "[" << b[i] << "] ";
	}
	cout << endl;


	cout << "\n\nDecimal sorting" << endl; 
	cout << "\tFloat" << endl;	//sort float
	float c[5] = { 3.3,3.2,1.1,1.2,2.3 };
	cout << "Original array : ";
	for (int i = 0; i < 5; i++)
	{
		cout << "[" << c[i] << "] ";
	}
	sort(c, 5);
	cout << "\nSorted array : ";
	for (int i = 0; i < 5; i++) 
	{
		cout << "[" << c[i] << "] ";
	}
	cout << endl;

	cout << "\tDouble" << endl; //sort double
	double d[5] = { 3.55555555,3.41111111,2.55555586,2.48888886,9.11111111 };
	cout << "Original array : ";
	for (int i = 0; i < 5; i++)
	{
		cout << "[" << d[i] << "] ";
	}
	sort(d, 5);
	cout << "\nSorted array : ";
	for (int i = 0; i < 5; i++) 
	{
		cout << "[" << d[i] << "] ";
	}
	cout << endl<<endl;
	system("Pause");
}